from django.urls import path, include
from rest_framework.routers import DefaultRouter
from .views import RentalViewset
from . import views



# from .views import index
router = DefaultRouter()
router.register('', RentalViewset, basename="rentalviewset")

urlpatterns = [
    path('', include(router.urls)),

]