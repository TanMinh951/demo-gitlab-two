import django_filters
from .models import Rental

class rentalFilter(django_filters.FilterSet):
    id = django_filters.NumberFilter(field_name='rental_id',lookup_expr='icontains')
    staff = django_filters.NumberFilter(field_name='staff_id', lookup_expr='exact')
    time_rental_start = django_filters.DateFilter(field_name='rental_date', lookup_expr='date__gt')
    time_rental_end = django_filters.DateFilter(field_name='rental_date', lookup_expr='date__lt')
    class Meta:
        model = Rental
        fields = ['id','staff', 'time_rental_start','time_rental_end']