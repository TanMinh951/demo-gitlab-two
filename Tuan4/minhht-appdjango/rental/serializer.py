from .models import Rental, Inventory,Address, Store, Staff, Customer
from rest_framework import serializers


# class StaffSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = Staff
#         fields = '__all__'
        
# class AddressSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = Address
#         fields = '__all__'

# class StoreSerializer(serializers.ModelSerializer):
#     address = AddressSerializer()
#     manager_staff =StaffSerializer()
#     class Meta:
#         model = Store
#         fields = ['store_id', 'address','manager_staff','last_update']     

# class InventorySerializer(serializers.ModelSerializer):
#     store = StoreSerializer()
#     class Meta:
#         model = Inventory
#         fields = ['inventory_id', 'film','store','last_update']

# class CustomerSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = Customer
#         fields = '__all__'

# class RentalSerializer(serializers.ModelSerializer):
#     inventory = InventorySerializer()
#     staff = StaffSerializer()
#     customer = CustomerSerializer()
#     class Meta:
#         model = Rental
#         fields =['rental_id','rental_date','customer','inventory','return_date','last_update','staff']


class RentalSerializer(serializers.ModelSerializer):
    class Meta:
        model = Rental
        fields = "__all__"
