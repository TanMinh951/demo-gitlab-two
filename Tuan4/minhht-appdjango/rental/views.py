from django.shortcuts import render
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.filters import OrderingFilter, SearchFilter
from .models import Rental
from .rentalFilter import rentalFilter
from rest_framework import status, viewsets
from .serializer import RentalSerializer
from rest_framework import filters
# Create your views here.

class RentalViewset(viewsets.ModelViewSet):
    queryset = Rental.objects.all()
    serializer_class = RentalSerializer
    filterset_class = rentalFilter